import type Table from "@/types/Table";
import http from "./axios";
function getTables() {
  return http.get("/tables");
}

function getTable(id: number) {
  return http.get("/tables/" + id);
}

function saveTable(table: Table) {
  return http.post("/tables", table);
}

function updateTable(id: number, table: Table) {
  return http.patch("/tables/" + id, table);
}

function deleteTable(id: number) {
  return http.delete("/tables/" + id);
}

export default { getTables, getTable, saveTable, updateTable, deleteTable };
