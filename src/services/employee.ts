import http from "@/services/axios";
import type Emp from "@/types/Employee";
function getEmp() {
  return http.get("/employees");
}

function saveEmployee(employee: Emp) {
  return http.post("/employees", employee);
}

function updateEmployee(id: number, employee: Emp) {
  return http.patch(`/employees/${id}`, employee);
}

function deleteEmployee(id: number) {
  return http.delete(`/employees/${id}`);
}

export default { getEmp, saveEmployee, updateEmployee, deleteEmployee };
