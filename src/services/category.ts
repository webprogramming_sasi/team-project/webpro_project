import type Category from "@/types/Category";
import http from "./axios";
function getCategories() {
  return http.get("/categories");
}

function getCategory(id: number) {
  return http.get("/categories/" + id);
}

function saveCategory(category: Category) {
  return http.post("/categories", category);
}

function updateCategory(id: number, category: Category) {
  return http.patch("/categories/" + id, category);
}

function deleteCategory(id: number) {
  return http.delete("/categories/" + id);
}

export default {
  getCategories,
  getCategory,
  saveCategory,
  updateCategory,
  deleteCategory,
};
