import type Menu from "@/types/Menu";
import http from "./axios";
function getFoods() {
  return http.get("/products");
}

function saveFood(food: Menu & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", food.name);
  formData.append("price", `${food.price}`);
  formData.append("name", food.type);
  formData.append("name", food.files[0]);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateFood(id: number, food: Menu) {
  return http.patch(`/products/${id}`, food);
}

function deleteFood(id: number) {
  return http.delete(`/products/${id}`);
}

export default { getFoods, saveFood, updateFood, deleteFood };
