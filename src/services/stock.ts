import type Stock from "@/types/Stock";
import http from "./axios";
function getStocks() {
  return http.get("/materials");
}

function saveStock(stock: Stock) {
  return http.post("/materials", stock);
}

function updateStock(id: number, stock: Stock) {
  return http.patch(`/materials/${id}`, stock);
}

function deleteStock(id: number) {
  return http.delete(`/materials/${id}`);
}

export default { getStocks, saveStock, updateStock, deleteStock };
