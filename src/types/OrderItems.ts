export default interface OrderItems {
  id?: number;
  name: string;
  quantity: number;
  createdAt?: Date;
  updatedAt?: Date;
  deleteAt?: Date;
}
