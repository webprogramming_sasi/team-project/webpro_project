export default interface Table {
  id?: number;
  number?: number;
  seats?: number;
  status?: string;
}
