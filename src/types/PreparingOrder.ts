export default interface PreparingOrder {
  id: number;
  name: string;
  tableNumber: string;
}
