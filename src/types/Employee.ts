export default interface Emp {
  id?: number;
  name: string;
  surname: string;
  tel: string;
  role: string;
  username: string;
  password: string;
}
