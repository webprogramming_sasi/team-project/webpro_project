export default interface Cart {
  id: number;
  name: string;
  // description?: string;
  quantity: number;
  price: Number;
}
