export default interface Stock {
  id?: number;
  name: string;
  minimum: number;
  remain: number;
  unit: string;
}
