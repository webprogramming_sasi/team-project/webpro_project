export default interface Menu {
  id?: number;
  name: string;
  price: number;
  type: string;
  categoryId?: number;
  image?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deleteAt?: Date;
}
