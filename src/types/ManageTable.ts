export default interface Mng {
  id: number;
  number: string;
  seats: string;
  status: string;
}
