export default interface QueueTableClean {
  id: number;
  tableNumber: string;
  status: string;
}
