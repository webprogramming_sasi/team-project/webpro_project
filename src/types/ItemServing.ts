export default interface ItemServing {
  id: number;
  name: string;
  quantity: number;
  status: string;
}
