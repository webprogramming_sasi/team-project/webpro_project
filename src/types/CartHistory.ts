export default interface CartHistory {
  id: number;
  name: string;
  quantity: number;
  status: string;
}
