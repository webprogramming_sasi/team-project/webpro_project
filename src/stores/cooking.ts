import { ref } from "vue";
import { defineStore } from "pinia";
import type PreparingOrder from "@/types/PreparingOrder";

export const useCookingOrderStore = defineStore("preparingOrder", () => {
  const cooking = ref<PreparingOrder[]>([
    { id: 1, name: "ไข่เจียว", tableNumber: "6" },
    { id: 2, name: "ผัดผักรวม", tableNumber: "6" },
    { id: 3, name: "ข้าวกะเพราหมูสับ", tableNumber: "4" },
    { id: 4, name: "ข้าวกะเพราหมูสับ", tableNumber: "4" },
  ]);
  return { cooking };
});
