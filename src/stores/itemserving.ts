import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import type ItemServing from "@/types/ItemServing";
type NewTable = Table & { items: ItemServing[] };

export const useItemserving = defineStore("itemServing", () => {
  const serving = ref<NewTable[]>([
    {
      id: 1,
      number: "2",
      seats: "4",
      status: "เปิดโต๊ะ",
      items: [
        {
          id: 1,
          name: "กะเพราหมูกรอบ",
          quantity: 1,
          status: "รอเสริฟ",
        },
        {
          id: 2,
          name: "ผักผัดรวม",
          quantity: 1,
          status: "รอเสริฟ",
        },
        {
          id: 3,

          name: "ไข่เจียวหมูสับ",
          quantity: 2,
          status: "รอเสริฟ",
        },
        {
          id: 4,
          name: "ข้าวกะเพราหมูสับ",
          quantity: 1,
          status: "ส่งครัว",
        },
      ],
    },
    {
      id: 5,
      number: "5",
      seats: "4",
      status: "เปิดโต๊ะ",
      items: [
        {
          id: 1,
          name: "กะเพราหมูชิ้น",
          quantity: 2,
          status: "รอเสริฟ",
        },
        {
          id: 2,
          name: "ผัดผักบุ้ง",
          quantity: 1,
          status: "รอเสริฟ",
        },
        {
          id: 3,

          name: "ไข่เจียว",
          quantity: 1,
          status: "รอเสริฟ",
        },
        {
          id: 4,
          name: "ข้าวกะเพราหมูสับ",
          quantity: 1,
          status: "ส่งครัว",
        },
        {
          id: 5,
          name: "เฉาก๊วยกังวาร",
          quantity: 2,
          status: "ส่งครัว",
        },
        {
          id: 6,
          name: "บัวลอยเจ้าเพื่อนยาก",
          quantity: 2,
          status: "ส่งครัว",
        },
      ],
    },
    {
      id: 12,
      number: "12",
      seats: "6",
      status: "เปิดโต๊ะ",
      items: [
        {
          id: 1,
          name: "กระเพราหมูกรอบ",
          quantity: 2,
          status: "รอเสริฟ",
        },
        {
          id: 2,
          name: "ผัดผักบุ้ง",
          quantity: 3,
          status: "รอเสริฟ",
        },
        {
          id: 3,

          name: "ไข่เจียว",
          quantity: 5,
          status: "รอเสริฟ",
        },
        {
          id: 4,
          name: "ข้าวกะเพราหมูสับ",
          quantity: 10,
          status: "ส่งครัว",
        },
        {
          id: 5,
          name: "เฉาก๊วยกังวาร",
          quantity: 2,
          status: "ส่งครัว",
        },
        {
          id: 6,
          name: "บัวลอยเจ้าเพื่อนยาก",
          quantity: 2,
          status: "ส่งครัว",
        },
      ],
    },
  ]);
  // const table: NewTable = {
  //   id: 1,
  //   number: "2",
  //   seats: "abc",
  //   status: "ว่าง",
  //   items: [
  //     {
  //       id: 1,
  //       name: "กรtเพรากบ",
  //       quantity: 1,
  //       status: "รอเสริฟ",
  //     },
  //     {
  //       id: 2,
  //       name: "ผักผัดรวม",
  //       quantity: 1,
  //       status: "รอเสริฟ",
  //     },
  //     {
  //       id: 3,

  //       name: "ไข่เจียว",
  //       quantity: 1,
  //       status: "รอเสริฟ",
  //     },
  //     {
  //       id: 4,
  //       name: "ข้าวกะเพราหมูสับ",
  //       quantity: 1,
  //       status: "ส่งครัว",
  //     },
  //   ],
  // };

  return { serving };
});
