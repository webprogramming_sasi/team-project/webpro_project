import { ref } from "vue";
import { defineStore } from "pinia";
import type CartHistory from "@/types/CartHistory";

export const useCartHistory = defineStore("cartHistory", () => {
  const cartHis = ref<CartHistory[]>([
    { id: 1, name: "ข้าวกะเพราหมูสับ", quantity: 2, status: "กำลังทำ" },
    {
      id: 2,
      name: "ข้าวกะเพราหมูสับ (ไม่เผ็ด)",
      quantity: 1,
      status: "กำลังทำ",
    },
    { id: 1, name: "ผัดผักรวม", quantity: 1, status: "กำลังทำ" },
    { id: 2, name: "น้ำเปล่า", quantity: 4, status: "เสิร์ฟแล้ว" },
  ]);
  return { cartHis };
});
