import { ref } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";
import stockService from "@/services/stock";
import { useLoadingStore } from "./loading";

export const useStockStore = defineStore("stock", () => {
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const delStockId = ref(-1);
  const editedStock = ref<Stock>({
    // id: -1,
    name: "",
    minimum: 0,
    remain: 0,
    unit: "",
  });
  // let lastId = 10;
  const stocks = ref<Stock[]>([
    // {
    //   id: 1,
    //   name: "ผักกาดขาว",
    //   min: 2,
    //   remain: 3,
    //   unit: "กิโลกรัม",
    // },
    // {
    //   id: 2,
    //   name: "แครอท",
    //   min: 2,
    //   remain: 5,
    //   unit: "กิโลกรัม",
    // },
    // {
    //   id: 3,
    //   name: "หัวไชเท้า",
    //   min: 2,
    //   remain: 2,
    //   unit: "กิโลกรัม",
    // },
  ]);

  async function getStocks() {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.getStocks();
      stocks.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // const deleteStock = (id: number): void => {
  //   deleteDialog.value = false;
  //   const index = stocks.value.findIndex((item) => item.id === id);
  //   stocks.value.splice(index, 1);
  // };

  const delConfirm = (id: number) => {
    deleteDialog.value = true;
    delStockId.value = id;
    console.log(delStockId.value);
  };

  // const saveStock = () => {
  //   if (editedStock.value.id < 0) {
  //     editedStock.value.id = lastId++;
  //     stocks.value.push(editedStock.value);
  //   } else {
  //     const index = stocks.value.findIndex(
  //       (item) => item.id === editedStock.value.id
  //     );
  //     stocks.value[index] = editedStock.value;
  //   }

  //   dialog.value = false;
  //   clear();
  // };

  async function saveStocks() {
    loadingStore.isLoading = true;
    try {
      if (editedStock.value.id) {
        const res = await stockService.updateStock(
          editedStock.value.id,
          editedStock.value
        );
      } else {
        const res = await stockService.saveStock(editedStock.value);
      }

      dialog.value = false;
      clear();
      await getStocks();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteStock(id: number) {
    loadingStore.isLoading = true;
    try {
      deleteDialog.value = false;
      const res = await stockService.deleteStock(id);
      await getStocks();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // const editStock = (stock: Stock) => {
  //   editedStock.value = { ...stock };
  //   dialog.value = true;
  // };

  function editStock(stock: Stock) {
    editedStock.value = stock;
    dialog.value = true;
  }

  // const clear = () => {
  //   editedStock.value = {
  //     id: -1,
  //     name: "",
  //     min: 0,
  //     remain: 0,
  //     unit: "",
  //   };
  // };

  function clear() {
    editedStock.value = {
      name: "",
      minimum: 0,
      remain: 0,
      unit: "",
    };
  }

  return {
    stocks,
    deleteStock,
    dialog,
    editedStock,
    clear,
    saveStocks,
    editStock,
    deleteDialog,
    delConfirm,
    getStocks,
  };
});
