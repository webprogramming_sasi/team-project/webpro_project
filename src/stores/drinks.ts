import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
const itemQuanrity = ref(1);

export const useDrinkStore = defineStore("drink", () => {
  const useDinkDialog = ref(false);
  const clear = () => {
    itemQuanrity.value = 1;
  };
  const drinks = ref<Menu[]>([
    { id: 1, name: "นำ้เปล่า (ขวดเล็ก)", price: 10, type: "drink" },
    { id: 2, name: "นำ้เปล่า (ขวดใหญ่)", price: 20, type: "drink" },
    { id: 3, name: "โค๊ก (ขวดเล็ก)", price: 15, type: "drink" },
    { id: 4, name: "โค๊ก (ขวดใหญ่)", price: 30, type: "drink" },
    { id: 5, name: "สไปรท์ (ขวดเล็ก)", price: 15, type: "drink" },
    { id: 6, name: "สไปรท์ (ขวดใหญ่)", price: 30, type: "drink" },
    { id: 7, name: "ขานม (ขวด)", price: 15, type: "drink" },
    { id: 8, name: "ชาเขียว (ขวด)", price: 15, type: "drink" },
    { id: 9, name: "เก๊กฮวย (ขวด)", price: 15, type: "drink" },
    { id: 10, name: "ชามะนาว (ขวด)", price: 15, type: "drink" },
    { id: 11, name: "นำ้แร่ (ขวดเล็ก)", price: 15, type: "drink" },
    { id: 12, name: "นำ้แร่ (ขวดใหญ่)", price: 35, type: "drink" },
  ]);
  return { drinks, useDinkDialog, clear, itemQuanrity };
});
