import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import tableService from "@/services/table";
import { useLoadingStore } from "@/stores/loading";

export const useTableStore = defineStore("table", () => {
  const loadingStore = useLoadingStore();
  const changeTableDialog = ref(false);
  const qrCodeDialog = ref(false);
  const canOpen = ref(false);
  const canMove = ref(false);
  const canCheckbill = ref(false);
  const dialog = ref(false);
  const delDialog = ref(false);
  // const selectedTable = ref<Table>({ number: 0, seats: 0, status: "" });
  const editedTable = ref<Table>({ number: 0, seats: 0, status: "" });
  const tables = ref<Table[]>([]);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedTable.value = { number: 0, seats: 0, status: "" };
    }
  });

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // function selectTable(table: Table) {
  //   console.log(table);
  //   selectedTable.value = JSON.parse(JSON.stringify(table));
  // }

  function editTable(table: Table) {
    console.log(table);
    editedTable.value = JSON.parse(JSON.stringify(table));
    dialog.value = true;
  }

  async function saveTable() {
    loadingStore.isLoading = true;
    try {
      // if (selectedTable.value.id) {
      //   const res = await tableService.updateTable(
      //     selectedTable.value.id,
      //     selectedTable.value
      //   );
      // } else if (editedTable.value.id) {
      //   const res = await tableService.updateTable(
      //     editedTable.value.id,
      //     editedTable.value
      //   );
      // }
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      } else {
        const res = await tableService.saveTable(editedTable.value);
      }
      dialog.value = false;
      await getTables();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function changeTable() {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      }
      changeTableDialog.value = false;
      await getTables();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // function editTable(table: Table) {
  //   console.log(table);
  //   editedTable.value = JSON.parse(JSON.stringify(table));
  //   dialog.value = true;
  // }

  async function deleteTable(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.deleteTable(id);
      delDialog.value = true;
      await getTables();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  const showButton = () => {
    if (editedTable.value.number != 0) {
      if (editedTable.value.status == "Available") {
        canOpen.value = true;
        canMove.value = false;
        canCheckbill.value = false;
      } else if (editedTable.value.status == "Unavailable") {
        canOpen.value = false;
        canMove.value = true;
        canCheckbill.value = true;
      } else if (editedTable.value.status == "Cleaning") {
        canOpen.value = false;
        canMove.value = false;
        canCheckbill.value = false;
      }
    }
  };

  function clearTables() {
    editedTable.value = { number: 0, seats: 0, status: "" };
  }

  return {
    tables,
    changeTableDialog,
    qrCodeDialog,
    showButton,

    getTables,
    clearTables,
    canOpen,
    canMove,
    canCheckbill,
    saveTable,
    dialog,
    deleteTable,
    editTable,
    editedTable,
    delDialog,
    changeTable,
  };
});
