import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Mng from "@/types/ManageTable";

export const useMngStore = defineStore("mng", () => {
  const mngs = ref<Mng[]>([
    {
      id: 1,
      number: "4",
      seats: "4",
      status: "ไม่ว่าง",
    },
  ]);
  return { mngs };
});
