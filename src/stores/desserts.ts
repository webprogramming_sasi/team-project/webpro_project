import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
const itemQuanrityDessert = ref(1);

export const useDessertStore = defineStore("dessert", () => {
  const useDessertDialog = ref(false);
  const clear = () => {
    itemQuanrityDessert.value = 1;
  };
  const desserts = ref<Menu[]>([
    { id: 1, name: "ลอดช่อง", price: 25, type: "drink" },
    { id: 2, name: "เฉาก๊วยนมสด", price: 25, type: "drink" },
    { id: 3, name: "บัวลอย", price: 25, type: "drink" },
    { id: 4, name: "สายไหม", price: 50, type: "drink" },
    { id: 5, name: "ข้าวเหนียวมะม่วง", price: 50, type: "drink" },
    { id: 6, name: "ข้าวเหนียวทุเรียน", price: 40, type: "drink" },
    { id: 7, name: "ข้าวหลาม", price: 30, type: "drink" },
    { id: 8, name: "ข้าวต้มมัด", price: 20, type: "drink" },
    { id: 9, name: "ไอศกรีมมะพร้าวนำ้หอม", price: 25, type: "drink" },
    { id: 10, name: "ไอศกรีมชาเขียว", price: 30, type: "drink" },
    { id: 11, name: "ไอศกรีมช็อกโกแลต", price: 30, type: "drink" },
    { id: 12, name: "ไอศกรีมสตรอว์เบอร์รี่", price: 30, type: "drink" },
  ]);
  return { desserts, useDessertDialog, clear, itemQuanrityDessert };
});
