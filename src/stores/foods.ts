import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import foodService from "@/services/food";
import type OrderItems from "@/types/OrderItems";
import { useLoadingStore } from "./loading";
import food from "@/services/food";
const itemQuanrity = ref<number>(1);

export const useFoodStore = defineStore("food", () => {
  const menuFood = "";
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const useDialogFood = ref(false);
  const foods = ref<Menu[]>([]);
  const orderItems = ref<OrderItems[]>([]);
  const itemes = ref<OrderItems>({ name: "", quantity: 0 });
  const selectedItem = ref<Menu>();
  const deleteDialog = ref(false);
  const editedProduct = ref<Menu & { files: File[] }>({
    name: "",
    price: 0,
    type: "",
    image: "No img product.png",
    files: [],
  });
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = {
        name: "",
        price: 0,
        type: "",
        image: "No img product.png",
        files: [],
      };
    }
  });
  const clear = () => {
    itemQuanrity.value = 1;
  };
  // async function getFoods() {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await foodService.getFoods();
  //     foods.value = res.data;
  //     console.log(res);
  //   } catch (e) {
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }
  async function getFoods() {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.getFoods();
      // foods.value = res.data;
      foods.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  const selectItem = (item: Menu) => {
    selectedItem.value = item;
    itemes.value.id = item.id; //เตรียมไว้ใส่ OrderIems
    return (itemes.value.name = item.name); //ใส่ quantity ตอนเลือก add ไปยัง orderItems
  };

  async function saveFoods() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await foodService.updateFood(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await foodService.saveFood(editedProduct.value);
      }
      dialog.value = false;
      await getFoods();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editProduct(food: Menu) {
    // editedProduct.value = food;
    dialog.value = true;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.deleteFood(id);
      await getFoods();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  return {
    foods,
    getFoods,
    useDialogFood,
    itemQuanrity,
    clear,
    itemes,
    selectItem,
    selectedItem,
    dialog,
    editedProduct,
    saveFoods,
    editProduct,
    deleteProduct,
  };
});
