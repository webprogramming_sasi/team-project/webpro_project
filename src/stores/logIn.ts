import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useEmpStore } from "./employee";
import { useLoadingStore } from "./loading";
// import router from "@/router";
export const useLoginStore = defineStore("login", () => {
  const loadingStore = useLoadingStore();
  const empStore = useEmpStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });

  const login = async (userName: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    if (empStore.username(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    }
    loadingStore.isLoading = false;
  };
  const logout = async () => {
    loadingStore.isLoading = true;
    loginName.value = "";
    localStorage.removeItem("loginName");
    loadingStore.isLoading = false;
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
// import { ref, computed } from "vue";
// import { defineStore } from "pinia";
// import { useEmpStore } from "./employee";

// export const useLoginStore = defineStore("login", () => {
//   const empStore = useEmpStore();
//   const loginName = ref("");
//   const isLogin = computed(() => {
//     // loginName is not empty
//     return loginName.value !== "";
//   });
//   const login = (userName: string, password: string): void => {
//     if()
//     loginName.value = userName;
//     localStorage.setItem("loginName", userName);
//   };
//   const logout = () => {
//     loginName.value = "";
//     localStorage.removeItem("loginName");
//   };
//   const loadData = () => {
//     loginName.value = localStorage.getItem("loginName") || "";
//   };

//   return { loginName, isLogin, login, logout, loadData };
// });
