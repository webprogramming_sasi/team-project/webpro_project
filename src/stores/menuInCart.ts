import { ref } from "vue";
import { defineStore } from "pinia";
import type useCartStore from "@/types/Cart";
export const useCartStore = defineStore("Cart", () => {
  const orderItem = ref<useCartStore[]>([
    { id: 1, name: "ข้าวกะเพราหมูสับ", quantity: 2, price: 90.0 },
    {
      id: 2,
      name: "ข้าวกะเพราหมูสับ (ไม่เผ็ด)",
      quantity: 1,
      price: 45.0,
    },
    { id: 3, name: "ผัดผักรวม", quantity: 1, price: 40.0 },
    { id: 4, name: "น้ำเปล่า", quantity: 4, price: 40.0 },
  ]);
  return { orderItem };
});
