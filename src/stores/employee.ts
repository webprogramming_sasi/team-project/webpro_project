import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Emp from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";

export const useEmpStore = defineStore("emp", () => {
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const emps = ref<Emp[]>([]);
  const editedEmployee = ref<Emp>({
    name: "",
    surname: "",
    tel: "",
    role: "",
    username: "",
    password: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        surname: "",
        tel: "",
        role: "",
        username: "",
        password: "",
      };
    }
  });

  async function getEmp() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmp();
      emps.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }
      dialog.value = false;
      await getEmp();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(employee: Emp) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmp();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  const username = (loginName: string, password: string): boolean => {
    const index = emps.value.findIndex((item) => item.username === loginName);
    if (index >= 0) {
      const emp = emps.value[index];
      if (emp.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };
  return {
    emps,
    username,
    getEmp,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
  };
});

// const lastId = 3;
// {
//   id: 1,
//   name: "Jin",
//   surname: "Jung",
//   tel: "0863661897",
//   role: "Manager",
//   username: "admin",
//   password: "Pass@1234",
// },
// {
//   id: 2,
//   name: "Naviya",
//   surname: "Sida",
//   tel: "0863661800",
//   role: "Chef",
//   username: "admin2",
//   password: "Pass@1234",
// },
// {
//   id: 3,
//   name: "Pi",
//   surname: "Po",
//   tel: "0863661899",
//   role: "waiter",
//   username: "admin3",
//   password: "Pass@1234",
// },
// {
//   id: 4,
//   name: "Dee",
//   surname: "Doo",
//   tel: "0863661902",
//   role: "Cashier",
//   username: "admin4",
//   password: "Pass@1234",
// },
// {
//   id: 5,
//   name: "Bi",
//   surname: "Kini",
//   tel: "0863661999",
//   role: "Assistant chef",
//   username: "admin5",
//   password: "Pass@1234",
// },
