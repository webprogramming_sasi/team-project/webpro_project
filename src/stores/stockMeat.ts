import { ref } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";

export const useStockMeatStore = defineStore("stockMeat", () => {
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const delStockId = ref(-1);
  const editedStockMeat = ref<Stock>({
    id: -1,
    name: "",
    minimum: 0,
    remain: 0,
    unit: "",
  });
  let lastId = 4;
  const stockMeats = ref<Stock[]>([
    {
      id: 1,
      name: "สันคอหมู",
      minimum: 5,
      remain: 8,
      unit: "กิโลกรัม",
    },
    {
      id: 2,
      name: "หมูบด",
      minimum: 5,
      remain: 5,
      unit: "กิโลกรัม",
    },
    {
      id: 3,
      name: "กุ้ง",
      minimum: 4,
      remain: 6,
      unit: "กิโลกรัม",
    },
  ]);

  const deleteStockMeat = (id: number): void => {
    deleteDialog.value = false;
    const index = stockMeats.value.findIndex((itemMeat) => itemMeat.id === id);
    stockMeats.value.splice(index, 1);
  };

  const delConfirm = (id: number) => {
    deleteDialog.value = true;
    delStockId.value = id;
    console.log(delStockId.value);
  };

  const saveStock = () => {
    if (editedStockMeat.value.id < 0) {
      editedStockMeat.value.id = lastId++;
      stockMeats.value.push(editedStockMeat.value);
    } else {
      const index = stockMeats.value.findIndex(
        (itemMeat) => itemMeat.id === editedStockMeat.value.id
      );
      stockMeats.value[index] = editedStockMeat.value;
    }

    dialog.value = false;
    clearStockMeat();
  };

  const editStockMeat = (stock: Stock) => {
    editedStockMeat.value = { ...stock };
    dialog.value = true;
  };

  const clearStockMeat = () => {
    editedStockMeat.value = {
      id: -1,
      name: "",
      minimum: 0,
      remain: 0,
      unit: "",
    };
  };
  return {
    stockMeats,
    deleteStockMeat,
    dialog,
    editedStockMeat,
    clearStockMeat,
    saveStock,
    editStockMeat,
    deleteDialog,
    delConfirm,
  };
});
