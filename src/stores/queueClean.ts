import { ref } from "vue";
import { defineStore } from "pinia";
import type QueueTable from "@/types/QueueTable";

export const useQueCleaningtable = defineStore("queuecleaningorder", () => {
  const QueueTable = ref<QueueTable[]>([
    { id: 1, tableNumber: "2", status: "รอเก็บโต๊ะ" },
    { id: 2, tableNumber: "10", status: "รอเก็บโต๊ะ" },
    { id: 3, tableNumber: "3", status: "รอเก็บโต๊ะ" },
    { id: 4, tableNumber: "4", status: "รอเก็บโต๊ะ" },
    { id: 5, tableNumber: "7", status: "รอเก็บโต๊ะ" },
    { id: 6, tableNumber: "8", status: "รอเก็บโต๊ะ" },
  ]);
  return { QueueTable };
});
