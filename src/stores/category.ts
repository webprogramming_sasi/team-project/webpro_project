import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import categoryService from "@/services/category";
import { useLoadingStore } from "./loading";

export const useCategoryStore = defineStore("category", () => {
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const categories = ref<Category[]>([]);
  const editedCategory = ref<Category>({
    name: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCategory.value = {
        name: "",
      };
    }
  });

  async function getCategory() {
    loadingStore.isLoading = true;
    try {
      const res = await categoryService.getCategories();
      categories.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveCategory() {
    loadingStore.isLoading = true;
    try {
      if (editedCategory.value.id) {
        const res = await categoryService.updateCategory(
          editedCategory.value.id,
          editedCategory.value
        );
      } else {
        const res = await categoryService.saveCategory(editedCategory.value);
      }
      dialog.value = false;
      await getCategory();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCategory(category: Category) {
    editedCategory.value = JSON.parse(JSON.stringify(category));
    dialog.value = true;
  }

  async function deleteCategory(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await categoryService.deleteCategory(id);
      await getCategory();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // const username = (loginName: string, password: string): boolean => {
  //   const index = categories.value.findIndex((item) => item.username === loginName);
  //   if (index >= 0) {
  //     const category = categories.value[index];
  //     if (category.password === password) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   return false;
  // };
  return {
    categories,

    getCategory,
    dialog,
    editedCategory,
    saveCategory,
    editCategory,
    deleteCategory,
  };
});

// const lastId = 3;
// {
//   id: 1,
//   name: "Jin",
//   surname: "Jung",
//   tel: "0863661897",
//   role: "Manager",
//   username: "admin",
//   password: "Pass@1234",
// },
// {
//   id: 2,
//   name: "Naviya",
//   surname: "Sida",
//   tel: "0863661800",
//   role: "Chef",
//   username: "admin2",
//   password: "Pass@1234",
// },
// {
//   id: 3,
//   name: "Pi",
//   surname: "Po",
//   tel: "0863661899",
//   role: "waiter",
//   username: "admin3",
//   password: "Pass@1234",
// },
// {
//   id: 4,
//   name: "Dee",
//   surname: "Doo",
//   tel: "0863661902",
//   role: "Cashier",
//   username: "admin4",
//   password: "Pass@1234",
// },
// {
//   id: 5,
//   name: "Bi",
//   surname: "Kini",
//   tel: "0863661999",
//   role: "Assistant chef",
//   username: "admin5",
//   password: "Pass@1234",
// },
