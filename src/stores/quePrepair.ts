import { ref } from "vue";
import { defineStore } from "pinia";
import type PreparingOrder from "@/types/PreparingOrder";

export const useQuePreparingStore = defineStore("preparingOrder", () => {
  const que = ref<PreparingOrder[]>([
    { id: 1, name: "ผัดผักรวม", tableNumber: "4" },
    { id: 2, name: "ข้าวกะเพราหมูสับ", tableNumber: "4" },
  ]);
  return { que };
});
